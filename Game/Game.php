<?php

namespace Game;

/**
 * Class Game
 * @package Game
 */
abstract class Game implements GameInterface
{
    const STATUS_ERROR     = 'ERROR';
    const STATUS_GAME_OVER = 'GAME_OVER';

    const RESULT_DRAW      = 'égalité';
    const RESULT_WIN       = 'gagne';
    const RESULT_LOSE      = 'perd';

    /**
     * @var $status
     */
    protected $status;

    /**
     * @var array $errors
     */
    protected $errors = [];

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed string
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param array $errors
     */
    public function setErrors(array $errors)
    {
        $this->errors = $errors;
    }

    /**
     * @param $error
     */
    public function addError($error)
    {
        $this->errors[] = $error;
    }

    /**
     * @return bool
     */
    public function hasError()
    {
        return !empty($this->errors);
    }
}