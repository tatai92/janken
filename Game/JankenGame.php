<?php

namespace Game;

use Entity\PlayerInterface;

/**
 * Class JankenGame
 * @package Game
 */
class JankenGame extends Game
{
    const HAND_ROCK = 1;
    const HAND_PAPPER = 2;
    const HAND_SCISSORS = 3;

    public static $validHands = [
        self::HAND_ROCK => 'Pierre',
        self::HAND_PAPPER => 'Feuille',
        self::HAND_SCISSORS=> 'Ciseaux'
    ];

    public static $results = [
        -2 => self::RESULT_WIN,
        -1 => self::RESULT_LOSE,
        0  => self::RESULT_DRAW,
        1  => self::RESULT_WIN,
        2  => self::RESULT_LOSE,
    ];

    /**
     * @var PlayerInterface
     */
    protected $player1;

    /**
     * @var PlayerInterface
     */
    protected $player2;

    /**
     * @param $playerHand
     * @return bool
     */
    public static function isValidHand($playerHand)
    {
        if (is_null($playerHand)) {
            return false;
        }

        return array_key_exists($playerHand, self::$validHands);
    }

    /**
     * @param $playerHand
     * @param $opponentHand
     * @return mixed
     */
    public static function calculatePlayerScore($playerHand, $opponentHand)
    {
        return $playerHand - $opponentHand;
    }

    /**
     * @param $playerScore
     * @return null
     */
    public static function calculatePlayerStatus($playerScore)
    {
        $results = self::$results;

        return isset($results[$playerScore]) ? $results[$playerScore] : null;
    }

    /**
     * Game constructor.
     * @param PlayerInterface $player1
     * @param PlayerInterface $player2
     */
    public function __construct(PlayerInterface $player1, PlayerInterface $player2)
    {
        $this->player1 = $player1;
        $this->player2 = $player2;
    }

    /**
     * run
     */
    public function run()
    {
        try {
            $this->setPlayersScores();
            $this->setPlayersStatus();
            $this->status = self::STATUS_GAME_OVER;

        } catch (\Exception $e) {
            // log $e->getMessage();
            $this->addError($e->getMessage());
            $this->status = self::STATUS_ERROR;
            return;
        }
    }

    /**
     * setPlayersScores
     * @throws \Exception
     */
    protected function setPlayersScores()
    {
        $player1Hand = $this->player1->getHand();
        $player2Hand = $this->player2->getHand();

        if (!self::isValidHand($player1Hand) || !self::isValidHand($player2Hand)) {
            throw new \Exception('Invalid Hand');
        }

        $this->player1->setScore(self::calculatePlayerScore($player1Hand, $player2Hand));
        $this->player2->setScore(self::calculatePlayerScore($player2Hand, $player1Hand));
    }

    /**
     * setPlayersStatus
     * @throws \Exception
     */
    protected function setPlayersStatus()
    {
        $player1Status = self::calculatePlayerStatus($this->player1->getScore());
        $player2Status = self::calculatePlayerStatus($this->player2->getScore());

        if (is_null($player1Status) || is_null($player2Status)) {
            throw new \Exception('Invalid Status');
        }

        $this->player1->setStatus($player1Status);
        $this->player2->setStatus($player2Status);
    }
}