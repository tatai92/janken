<?php

namespace Game;

/**
 * Interface GameInterface
 * @package Game
 */
interface GameInterface
{
    /**
     * @return mixed
     */
    public function run();

    /**
     * @return string
     */
    public function getStatus();

    /**
     * @return array
     */
    public function getErrors();

    /**
     * @return bool
     */
    public function hasError();
}