<?php require_once 'autoload.php'; ?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <title>Janken</title>
    </head>
    <body>
        <?php
            if (isset($_POST['hand'])) {
                require_once('result.php');
            } else {
                require_once('form.php');
            }
        ?>
    </body>
</html>
