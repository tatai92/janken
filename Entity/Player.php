<?php

namespace Entity;

/**
 * Class Player
 * @package Entity
 */
abstract class Player implements PlayerInterface
{
    /**
     * @var String $name
     */
    protected $name;

    /**
     * @var int $status
     */
    protected $status;

    /**
     * @return String
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }
}
