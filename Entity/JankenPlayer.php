<?php

namespace Entity;

/**
 * Class JankenPlayer
 * @package Entity
 */
class JankenPlayer extends Player implements JankenPlayerInterface
{
    /**
     * @var int $hand null
     */
    protected $hand;

    /**
     * @var int $score null
     */
    protected $score;

    /**
     * JankenPlayer constructor.
     */
    public function __construct($name = null, $hand = null)
    {
        $this->name = $name;
        $this->hand = $hand;
    }

    /**
     * @return null
     */
    public function getHand()
    {
        return $this->hand;
    }

    /**
     * @param null $hand
     */
    public function setHand($hand)
    {
        $this->hand = $hand;
    }

    /**
     * @return mixed
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * @param mixed $score
     */
    public function setScore($score)
    {
        $this->score = $score;
    }
}

