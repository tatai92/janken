<?php

namespace Entity;

/**
 * Interface JankenPlayerInterface
 * @package Entity
 */
interface JankenPlayerInterface extends PlayerInterface
{
    /**
     * @return mixed
     */
    public function getHand();

    /**
     * @param $hand
     * @return mixed
     */
    public function setHand($hand);

    /**
     * @return mixed
     */
    public function getScore();

    /**
     * @param $score
     * @return mixed
     */
    public function setScore($score);
}
