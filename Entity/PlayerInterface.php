<?php

namespace Entity;

/**
 * Interface PlayerInterface
 * @package Entity
 */
interface PlayerInterface
{
    /**
     * @return string
     */
    public function getName();

    /**
     * @param $name
     * @return string
     */
    public function setName($name);

    /**
     * @return int
     */
    public function getStatus();

    /**
     * @param $status
     * @return string
     */
    public function setStatus($status);
}
