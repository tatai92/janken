<?php

use Entity\JankenPlayer;
use Game\Game;
use Game\JankenGame;


$user = new JankenPlayer('Utilisateur', $_POST['hand']);

// set random hand for computer
$hands = array_keys(JankenGame::$validHands);
$computer = new JankenPlayer('Machine', $hands[array_rand($hands)]);

$game = new JankenGame($user, $computer);
$game->run();

// show errors
if ($game->hasError()) {
    foreach ($game->getErrors() as $error) {
        echo '<p>' . $error . '</p>';
    }
}

// show results
if ($game->getStatus() === Game::STATUS_GAME_OVER) {
    echo '<p>' . $user->getName() . ': '.$user->getStatus() . ' (' . JankenGame::$validHands[$user->getHand()] . ')' . '<p>';
    echo '<p>' . $computer->getName() . ': '.$computer->getStatus() . ' (' . JankenGame::$validHands[$computer->getHand()] . ')' . '<p>';
}

?>

<p>
    <a href '/'>Recommencez</a>
</p>
